**Installing**

## Server

You can install and run laravel server using following commands in server direct

1. `composer install`
2. `php artisan serve`


---

## Client

In the client directory

1. `npm install`
2. `npm start`
